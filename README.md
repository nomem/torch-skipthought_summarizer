# Torch Skipthought Summarizer #

This is the torch implementation of the skipthought summarizer.


### Setup ###
This project requires the dataset and torch. Here is the instruction for running the project:

```
# Clone this repo first
git clone https://nomem@bitbucket.org/nomem/torch-skipthought_summarizer.git
# Enter the directory
cd torch-skipthought_summarizer

# Download the neuralsum data from here : https://docs.google.com/uc?id=0B0Obe9L1qtsnSXZEd0JCenIyejg&export=download
# save cookies
mkdir neuralsum_data
wget --save-cookies cookies.txt --keep-session-cookies --no-check-certificate 'https://docs.google.com/uc?export=download&id=0B0Obe9L1qtsnSXZEd0JCenIyejg' -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/Code: \1\n/p'
# use the code in YOURCODEID from previous command
wget --load-cookies cookies.txt 'https://docs.google.com/uc?export=download&confirm=YOURCODEID&id=0B0Obe9L1qtsnSXZEd0JCenIyejg' -O neuralsum_data/neuralsum.zip
cd neuralsum_data
unzip neuralsum.zip
# Go back
cd ..

```


### Contact ###
Reach me at momen@vt.edu