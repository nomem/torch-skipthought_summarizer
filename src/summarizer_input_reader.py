import torch
import argparse

import spacy
from torchtext import data

torch.manual_seed(1)


class InputReader(object):
	"""docstring for InputReader"""
	def __init__(self, arg):
		super(InputReader, self).__init__()
		self.spacy_en = spacy.load('en')

		def tokenize2(x):
			return [tok.text for tok in self.spacy_en.tokenizer(x)]
		self.TEXT = data.Field(sequential=True, tokenize=tokenize2, lower=True)
		self.LABEL = data.Field(sequential=False, use_vocab=False)
		self.input_dir = arg.input_dir
		self.train_file = arg.input_extension + "_training.csv"
		self.test_file = arg.input_extension + "_test.csv"
		self.val_file = arg.input_extension + "_validation.csv"
		self.device = None
		if arg.device == "GPU" and torch.cuda.is_available():
			self.device = torch.cuda()

	def load(self):
		train, val, test = data.TabularDataset.splits(path=self.input_dir, train=self.train_file, validation=self.val_file, test=self.test_file, format='tsv', fields=[('id', None), ('label', self.LABEL), ('text', self.TEXT), ('filename', None)], skip_header=True)
		self.train = train
		self.test = test
		self.val = val

	def build_vocab(self):
		self.TEXT.build_vocab(self.train)
		self.vocab = self.TEXT.vocab

	def build_iterators(self):
		train_iter, val_iter, test_iter = data.Iterator.splits((self.train, self.val, self.test), sort_key=lambda x: len(x.Text), batch_sizes=(32, 256, 256), device=self.device, sort_within_batch=True, repeat=False)
		self.train_iter = train_iter
		self.test_iter = test_iter
		self.val_iter = val_iter

	def run(self):
		self.load()
		self.build_vocab()
		self.build_iterators()


def test_input_reader(args):
	ir = InputReader(args)
	ir.run()
	print(len(ir.vocab))


def main():
	parser = argparse.ArgumentParser(description='Torch Skipthought Summarizer Model')
	parser.add_argument("-id", "--input-dir", type=str, default="data/", required=False, help='data directory')
	parser.add_argument("-ie", "--input-extension", type=str, default="neuralsum", required=False, help='file extension start')
	parser.add_argument("-d", "--device", type=str, default="CPU", required=False, help='GPU or CPU')
	args = parser.parse_args()
	test_input_reader(args)


if __name__ == '__main__':
	main()
