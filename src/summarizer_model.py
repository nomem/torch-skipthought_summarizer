import torch
from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import sys
import argparse


torch.manual_seed(1)



class Summarizer_model(nn.Module):
	"""docstring for Summarizer_model"""
	def __init__(self, Biskip=True, args):
		super(Summarizer_model, self).__init__()
		self.vocab_size = args.vocab_size
		self.embedding_dim = args.embedding_dim
		self.encoder_dim = args.encoder_dim
		self.decoder_dim = args.output_dim
		self.num_classes = args.num_classes
		self.word_embeddings = nn.Embedding(self.vocab_size, self.embedding_dim)
		self.encoder = nn.gru(self.embedding_dim,self.encoder_dim)
		self.decoder = nn.gru(self.encoder_dim, self.decoder_dim)
		self.linear = nn.Linear(self.decoder_dim, self.num_classes)
		self.encoder_w = self.init_hidden_w()

	def init_hidden_w(self):
		return (torch.zeros(1, 1, self.hidden_dim), torch.zeros(1, 1, self.hidden_dim))

	def forward(self, sentence):
		embeds = self.word_embeddings(sentence)
		encoder_out, self.encoder_w = self.encoder(embeds.view(len(sentence), 1, -1), self.encoder_w)
		decoder_out, decoder_w = self.decoder(embeds.view(len(encoder_out), 1, -1), self.encoder_w)				


class Test_model(object):
	"""docstring for Test_model"""
	def __init__(self, args):
		super(Test_model, self).__init__()
		self.test_name = arg.test_name.lower()
		self.config = {"vocab_size":20000, "embedding_dim":620, "encoder_dim":4800, "decoder_dim": 4800, "num_classes": 2}
		for key in self.config:
			if key in args:
				self.config[key] = args[key]


	def test_model_create(self):
		pass

	def test_model_load_pretrained_weights(self):
		pass

	def test_model_forward(self):
		pass

	def test_model_backward(self):
		pass

	def test_weight_dims(self):
		pass

	def test_zero_or_infinity(self):
		pass

	def create_model(self):
		self.model = Summarizer_model(Biskip=False, self.config)

	def run(self):
		methods = [func for func in dir(Test_model) if callable(getattr(Tester, func)) and 'test_' == func[:5]]
		self.create_model()
		if self.test_name == "all":
			for method in methods:
				print("Testing method: %s" %(method))
				getattr(self, method)(self.model)
				print("Result: success")
		else:
			if self.test_name in methods:
				print("Testing method: %s" %(self.test_name))
				getattr(self, self.test_name)(self.model)
				print("Result: success")




def main():
	parser = argparse.ArgumentParser(description='Torch Skipthought Summarizer Model')
	parser.add_argument("-t", "--test-name", type=str, default="all", required=False, help='Name of the test function')
	parser.add_argument("-d", "--device", type=str, default="CPU", required=False, help='GPU or CPU')
	args = parser.parse_args()
	test_model = Test_model(args)
	test_model.run()



if __name__ == '__main__':
	main()