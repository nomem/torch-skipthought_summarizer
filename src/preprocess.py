import pandas
import sys
import argparse
from os import listdir
from os.path import isfile, join, isdir
from multiprocessing import Pool
from functools import partial
import multiprocessing
import codecs


# constants
SOURCES = ["cnn", "dailymail"]
DATASET_TYPES = ["training", "test", "validation"]


def read_file(filename, cur_dir, source):
	"""
	Read a file and return the pandas dataframe
	"""
	print("reading directory %s file: %s" % (cur_dir, filename))
	texts = []
	labels = []
	skip_lines = 3
	cnt = 0
	try:
		file = codecs.open(join(cur_dir, filename), 'r', 'utf-8', errors="ignore")
		for line in file:
			cnt += 1
			if cnt <= skip_lines:
				continue
			if line == "\n" or line == "":
				break
			line = line.strip()
			line = line.replace('}', '').replace('{', '').replace('|', '').replace(',','')
			line = line.replace('<unk>', ' | ')
			text, label = line.split('\t\t\t')
			label = int(label.split("\n")[0])
			if label == 2:
				label = 0
			texts.append(text)
			labels.append(label)
		data_dict = {
			"text": texts,
			"label": labels
		}
		dataframe = pandas.DataFrame(data=data_dict)
		dataframe["filename"] = source+"_"+filename.split(".")[0]
		return dataframe
	except Exception as e:
		print(e)
		data_dict = {
			"text": [],
			"label": [],
			"filename":[]
		}
		return pandas.DataFrame(data=data_dict)



def preprocess(args):
	for dataset_type in DATASET_TYPES:
		dataframe_list = []
		for source in SOURCES:
			if isdir(join(args.input_dir,source)) and isdir(join(join(args.input_dir, source), dataset_type)):
				cur_dir = join(join(args.input_dir, source), dataset_type)
				files = [file for file in listdir(cur_dir) if isfile(join(cur_dir, file)) and len(file) > 16 and file[-8:] == ".summary"]
				pool = Pool(processes=multiprocessing.cpu_count())
				dataframe_list += pool.map(partial(read_file, cur_dir=cur_dir, source=source), files)
			else:
				print("Either %s or %s doesn't exist" % (join(args.input_dir, source), join(join(args.input_dir, source), dataset_type)))
		dataframe = pandas.concat(dataframe_list)
		dataframe["text"] = dataframe["text"].astype(str)
		dataframe["filename"] = dataframe["filename"].astype(str)
		dataframe["label"] = dataframe["label"].astype(int)
		dataframe.to_csv(args.output_dir+"/"+args.output_extension+"_"+dataset_type+".csv", sep='\t')



def main():
	parser = argparse.ArgumentParser(description='Torch Skipthought Preprocessing Input')
	parser.add_argument("-id", "--input-dir", type=str, default="neuralsum_data/neuralsum", required=False, help='Input directory')
	parser.add_argument("-od", "--output-dir", type=str, default="data", required=False, help='Output diretory')
	parser.add_argument("-oe", "--output-extension", type=str, default="neuralsum", required=False, help='Output extension')
	args = parser.parse_args()
	preprocess(args)



if __name__ == '__main__':
	main()

